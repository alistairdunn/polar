
a<-c("polar.1000m","polar.100m","polar.1200m","polar.1500m","polar.2000m","polar.200m","polar.2500m",
     "polar.3000m","polar.300m","polar.4000m","polar.400m","polar.5000m","polar.500m","polar.550m",
     "polar.6000m","polar.600m","polar.700m","polar.800m","polar.900m","polar.ASD.lines","polar.coast.lines",
     "polar.iceshelf.lines","polar.SSRU.lines","polar.subarea.lines","polar.tongue.lines","subarea.881",
     "subarea.881v2","subarea.881v3","subarea.881v4","subarea.882","subarea.882v1","subarea.882v2",
     "subarea.882v3","subarea.882v4")

for(i in 1:length(a)) {
  save(list=a[i],file=paste(a[i],".rda",sep=""))
}


