subarea.881v2 <-
structure(list(long = c(NA, 150L, 150L, 190L, 190L, 150L, NA,
150L, 150L, 180L, 180L, 150L, NA, 180L, 180L, 190L, 190L, 180L,
NA, 160L, 160L, 180L, 180L, 160L, NA, 180L, 180L, 190L, 190L,
180L), lat = c(NA, -60L, -65L, -65L, -60L, -60L, NA, -65L, -72L,
-72L, -65L, -65L, NA, -65L, -72L, -72L, -65L, -65L, NA, -72L,
-82L, -82L, -72L, -72L, NA, -72L, -82L, -82L, -72L, -72L), area = c(88.1,
88.1, 88.1, 88.1, 88.1, 88.1, 88.1, 88.1, 88.1, 88.1, 88.1, 88.1,
88.1, 88.1, 88.1, 88.1, 88.1, 88.1, 88.1, 88.1, 88.1, 88.1, 88.1,
88.1, 88.1, 88.1, 88.1, 88.1, 88.1, 88.1), SSRU = structure(c(1L,
1L, 1L, 1L, 1L, 1L, 2L, 2L, 2L, 2L, 2L, 2L, 3L, 3L, 3L, 3L, 3L,
3L, 4L, 4L, 4L, 4L, 4L, 4L, 5L, 5L, 5L, 5L, 5L, 5L), .Label = c("A",
"B", "C", "D", "E"), class = "factor")), .Names = c("long", "lat",
"area", "SSRU"), class = "data.frame", row.names = c(NA, -30L
))
