# Management areas

#N70
x1<-c(150,150,160,160,173.75,173.75,210,210,150,NA)
y1<-c(-60,-62.5,-62.5,-65,-65,-70,-70,-60,-60,NA)
label1<-rep("N70",length(x1))

#S70
x2<-c(173.75,173.75,190,190,210,210,173.75,NA)
y2<-c(-70,-73.5,-73.5,-72,-72,-70,-70,NA)
label2<-rep("S70",length(x2))

#SRZ
x3<-c(180,180,190,190,196,196,190,190,180,NA)
y3<-c(-76,-73.5,-73.5,-75,-75,-76.5,-76.5,-76,-76,NA)
label3<-rep("SRZ",length(x3))

"polar.management.area.lines"<-data.frame("management.area"=c(label1,label2,label3),"x"=c(x1,x2,x3),"y"=c(y1,y2,y3),"lat"=c(y1,y2,y3),"long"=c(x1,x2,x3))
#"management.area.lines"<-data.frame("x"=c(150,150,210,210,150),"y"=c(-60,-70,-70,-60,-60),"lat"=c(150,150,210,210,150),"long"=c(-60,-70,-70,-60,-60))

save("polar.management.area.lines",file=paste("C:\\Users\\alist\\OneDrive\\Projects\\Software\\polar\\R-libraries\\polar\\data\\polar.management.area.lines.rda",sep=""))

