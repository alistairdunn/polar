#########################################################################################################
#
# Generate depth contours from the 2019 GEBCO data (https://www.gebco.net/)
# using GMT (https://www.soest.hawaii.edu/gmt/) 
#
# uses gmt grdcontour, gmt connect, gmt simplify, and sed utility
# run in cmd on Windows, but easily converted to Linux
#
# Script to create x,y files, with polygons seperated by NAs. Doesn't do simple features
#########################################################################################################

#########################################################################################################
# Set working DIR for GEBCO
DIR<-"c:/Projects/GEBCO/2019/"

# Define contours to extract
cont<-c(0,100,200,300,400,500,550,600,650,700,750,800,850,900,950,1000,1250,1500,2000,2500,3000,3500,4000,4500,5000,5500,6000)
#cont<-c(100)

# Write these as definition files to external files for use by GMT grdcontour
for(i in 1:length(cont)) {
  x<-file(paste(DIR,"c",cont[i],".def",sep=""))
  cat("-",cont[i],"\n",sep="",file=x)
  close(x)
}

# Write a external batch file to process all of them
unlink(paste(DIR,"Build.bat",sep=""))
x<-file(paste(DIR,"Build.bat",sep=""),"w")
for(i in 1:length(cont)) {
  # Extract the contour
  cat("grdcontour GEBCO_2019.nc -C",paste("c",cont[i],".def",sep="")," -R0/360/-90/-30 -B0+360-90-30 -JX10 -Fl -D",
      paste("c",cont[i],".gmt",sep="")," > ",paste("c",cont[i],".log",sep=""),"\n",file=x,sep="")
  cat("gmt connect ",paste("c",cont[i],".gmt",sep="")," > ",paste("c",cont[i],".gmt2\n",sep=""),file=x,sep="")
  cat("gmt simplify ",paste("c",cont[i],".gmt2",sep="")," -T2000e > ",paste("c",cont[i],".gmt3\n",sep=""),file=x,sep="")
  cat("sed \"s/>/NA NA NA/g\" ", paste("c",cont[i],".gmt3",sep="")," > ",paste("c",cont[i],".dat\n",sep=""),file=x,sep="")
}  
close(x)

#########################################################################################################
# Run build.bat in DIR - run this from the command line in the GEBCO directory
# build.bat

#########################################################################################################
# read the generated contours back into  R
for(i in 1:length(cont)) {
  res<-read.table(paste(DIR,"c",cont[i],".dat",sep=""),header=F,as.is=T,flush=T,col.names=c("x","y","z"),skip=1)[,1:2]
  names(res)<-c("x","y")
  res$x<-as.numeric(res$x)
  res$y<-as.numeric(res$y)
  res$x<-ifelse(res$x<0,res$x+360,res$x)
  assign(paste("polar.",cont[i],"m",sep=""),res,pos=1)
}

#########################################################################################################
# Write to dump file
DIR2<-"C:/Users/alist/OneDrive/Projects/Software/polar/R-libraries/polar/data"

a <- objects(pattern = "polar.*m")
for (i in 1:length(a)) {
  save(list=a[i],file=paste(DIR2,a[i],".rda",sep=""))
}
a
rm(a)

#########################################################################################################
# Clean up the directory of all temporary files
DIR<-"c:/Projects/GEBCO/2019/"
for(i in 1:length(cont)) {
  unlink(paste(DIR,"c",cont[i],".def",sep=""))
  unlink(paste(DIR,"c",cont[i],".log",sep=""))
  unlink(paste(DIR,"c",cont[i],".gmt",sep=""))
  unlink(paste(DIR,"c",cont[i],".gmt2",sep=""))
  unlink(paste(DIR,"c",cont[i],".gmt3",sep=""))
}  


# Write to dump file
PATH<-"c:\\Projects\\General\\Polar Library\\polar\\R\\"

a <- objects(pattern = "polar.*m")
for (i in 1:length(a))
  dump(a[i],fileout=paste(PATH,a[i],".R",sep=""))
dump(c("polar.coast.lines","polar.iceshelf.lines","polar.tongue.lines"), fileout=paste(PATH,"polar.data.R",sep=""))
dump("polar.ASD.lines", fileout=paste(PATH,"polar.ASD.lines.R",sep=""))
dump("polar.SSRU.lines", fileout=paste(PATH,"polar.SSRU.lines.R",sep=""))
dump("polar.SSRU.labels", fileout=paste(PATH,"polar.SSRU.labels.R",sep=""))
dump(c("subarea.88.1","subarea.88.2"), fileout=paste(PATH,"polar.subarea.R",sep=""))


