library(rgdal)

make.filename<-function(file="",path="",add.terminal=F) {
  if(path != "") {
    plc <- substring(path, nchar(path))
    if(!(plc == "\\" | plc == "/")) path <- paste(path, "\\", sep = "")
  }
  filename <- paste(path, file, sep = "")
  if(add.terminal==T) {
    plc <- substring(filename, nchar(filename))
    if(!(plc == "\\" | plc == "/")) filename <- paste(filename, "\\", sep = "")
  }
  return(filename)
}

DIR<-list("Data"="c:/users/Alist/OneDrive/Projects/Software/Polar/Build data")
DIR$polar<-"C:/Users/alist/OneDrive/Projects/Software/polar/R-libraries/polar/data"

#CEMP sites
ogrInfo(make.filename("gis_cemp_sites",DIR$Data),"cemp_sites")
# read in shapefiles
a <- readOGR(make.filename("gis_cemp_sites",DIR$Data), "cemp_sites")
cemp.sites<-data.frame(lat=a$latitude,long=a$longitude,name=a$"site_name",nation=a$nationalit,code=a$"site_code")

# Subareas
ogrInfo(make.filename("gis_subareas",DIR$Data),"subareas")
# read in shapefiles
a <- readOGR(make.filename("gis_subareas",DIR$Data),"subareas")
subareas<-data.frame(lat=a$latitude,long=a$longitude,name=a$"site_name",nation=a$nationalit,code=a$"site_code")

#Research Blocks
library(shapefiles)
a<-read.shapefile(make.filename("rb-shapefile-WGS84/rb-shapefile-WGS84",DIR$Data))
length(a)
x1<-a[3]$dbf$dbf$GAR_Short
x2<-a[1]$shp[1]$shp
res<-list()
for(i in 1:length(x2)) {
  tmp<-rbind(x2[[i]]$points,c(NA,NA))
  res[[i]]<-tmp
  res[[i]]$x<-tmp$X
  res[[i]]$long<-tmp$X
  res[[i]]$y<-tmp$Y
  res[[i]]$lat<-tmp$Y
  res[[i]]$block<-x1[i]
}
res<-do.call(rbind,res)
res$season<-"2018"
res[,!names(res)=="X"]
res[,!names(res)=="Y"]
polar.RB.lines<-res
save(polar.RB.lines,file=make.filename("polar.RB.lines.rda",DIR$polar))

#EEZs
library(shapefiles)
a<-read.shapefile(make.filename("eez-shapefile-WGS84_0/eez-shapefile-WGS84",DIR$Data))
length(a)
x1<-a[3]$dbf$dbf$GAR_Short
x2<-a[1]$shp[1]$shp
res<-list()
for(i in 1:length(x2)) {
  tmp<-rbind(x2[[i]]$points,c(NA,NA))
  res[[i]]<-tmp
  res[[i]]$x<-tmp$X
  res[[i]]$long<-tmp$X
  res[[i]]$y<-tmp$Y
  res[[i]]$lat<-tmp$Y
  res[[i]]$block<-x1[i]
}
res<-do.call(rbind,res)
res$season<-"2018"
res[,!names(res)=="X"]
res[,!names(res)=="Y"]
polar.EEZ.lines<-res
save(polar.EEZ.lines,file=make.filename("polar.EEZ.lines.rda",DIR$polar))

