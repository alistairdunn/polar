#' polarPlot
#'
#' @export
#'
"polar.depth" <-
function(contour = 1000, ...)
{
# Alistair Dunn: 1 October 2004
  cnam <- paste("polar.", contour, "m", sep = "")
  if(exists(cnam))
    polar.lines(get(cnam)$x, get(cnam)$y, err=-1, great.circle=FALSE, ...)
  else {
    cat("Selected depth contour does not exist\n")
  }
  invisible()
}
