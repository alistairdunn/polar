#' polarPlot
#'
#' @export
#'
"polar.coastline" <-
function(add.coastline=T,add.iceshelf=T,fill=F,coast.fill.col="ivory3",iceshelf.fill.col="ivory2",...) {
# Alistair Dunn: 1 October 2004
  n<-.polar$n
  xlim<-.polar$xlim
  ylim<-.polar$ylim
  limit<-.polar$limit
  rotate<-.polar$rotate
  if(add.iceshelf) {
    res<-polar.stereographic(polar.iceshelf.lines$x,polar.iceshelf.lines$y,limit,rotate)
    if(fill) polygon(res$x,res$y,col=iceshelf.fill.col,err=-1)
    lines(res$x,res$y,err=-1,...)
  }
  if(add.coastline) {
    res<-polar.stereographic(polar.coast.lines$x,polar.coast.lines$y,limit,rotate)
    if(fill) polygon(res$x,res$y,col=coast.fill.col,err=-1)
    lines(res$x,res$y,err=-1,...)
  }
  invisible()
}
