#' polarPlot
#'
#' Converts a set of positions in latN,lonE to x-y coords for plotting on a stereographic projection centered at the S pole and ranging between latitudes latlim[2] & latlim[1].
#' All latitudes are assumed to be degrees N (so southern hemisphere has negative latitudes) and longitudes are assumed to be degrees E.
#' Coordinates for positions outside the latitude range, latlim, are set to NA
#' Returns a list with components x and y are such that the S pole is (0,0) and the circle latN=latlim[2] has radius 1.
#' Based on code by R.I.C.C Francis
#' @author Alistair Dunn
#'
#' @export
#'
"polar.stereographic" <-
function(long, lat,limit=c(-90, 0),rotate)
{
 if((length(lat)/length(long)) %% 1 != 0 & (length(long)/length(lat)) %% 1 != 0) stop("lat and long are of incompatible lengths")
 deg.to.rad <- pi/180
 inside <- !is.na(lat) & lat >= limit[1] & lat <= limit[2]
 lat.rad <- lat[inside] * deg.to.rad
 lon.rad <- (long[inside]+rotate+180) * deg.to.rad
 lat.rad.max <- limit[2] * deg.to.rad
 rho <- (cos(lat.rad)/(1 - sin(lat.rad)))/(cos(lat.rad.max)/(1 - sin(lat.rad.max)))
 x <- y <- rep(NA, max(c(length(lat), length(long))))
 x[inside] <-  - rho * sin(lon.rad)
 y[inside] <-  - rho * cos(lon.rad)
 return(list(x = x, y = y))
}
