#' polarPlot
#'
#' @export
#'
"polar.WorldCoastline" <-
function(...,label=F,fill=NULL,add=T) {
  res<-polar.WorldCoastline.lines
  if(add) {
    if(!is.null(fill)) {
      polar.polygon(res$long,res$lat,col=fill,great.circle=T,...)
    }
    polar.lines(res$long,res$lat,great.circle=T,...)
 }
  invisible(res)
}

