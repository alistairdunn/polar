#' polarPlot
#'
#' @export
#'
"polar.lines.great.circle" <-
function(x,y,n=.polar$n,...) {
# Alistair Dunn: 1 October 2004
  n.default<-n
  n<-.polar$n
  xlim<-.polar$xlim
  ylim<-.polar$ylim
  limit<-.polar$limit
  rotate<-.polar$rotate
  if(length(x)!=length(y)) stop("the lengths of x and y must be equal")
  if(length(x)<2) stop("the lengths of x and y must be at least 2")
  for(i in 2:length(x)) {
    if(!(is.na(x[i]) || is.na(x[i-1]) || is.na(y[i]) || is.na(y[i-1]))) {
      if((!is.na(x[i]) && !is.na(x[i-1]) && (x[i]-x[i-1]) < 0.005) 
	     && (!is.na(y[i]) && !is.na(y[i-1]) && (y[i]-y[i-1]) < 0.005)) 
		 N=n.default
      else N=n
	  X<-seq(x[i-1],x[i],length=N)
      Y<-seq(y[i-1],y[i],length=N)
      res<-polar.stereographic(X,Y,limit,rotate)
      lines(res$x,res$y,...)
    }
  }
  invisible()
}
