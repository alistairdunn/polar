#' polarPlot
#'
#' @export
#'
"polar.axis" <-
function(side=c(1,2), at, labels=T, ticks=T, line=0, ...) {
# Alistair Dunn: 1 October 2004
  n<-.polar$n
  xlim<-.polar$xlim
  ylim<-.polar$ylim
  limit<-.polar$limit
  rotate<-.polar$rotate
  par(xpd=T)
  if(missing(at)) {
    xl<-pretty(.polar$xlim)
    xl<-xl[xl >= min(xlim) & xl <= max(xlim)]
    yl<-pretty(ylim)
    yl<-yl[yl >= min(ylim) & yl <= max(ylim)]
  } else {
    if(side %in% c(1,3)) xl<-at
    if(side %in% c(2,4)) yl<-at
  }
  if(1 %in% side) {
    label<-ifelse(xl>180,paste(360-xl,sep=""),xl)
    label<-ifelse(xl<180,paste(xl,"\u00B0E",sep=""),ifelse(xl==180,paste(label,"\u00B0",sep=""),paste(label,"\u00B0W",sep="")))
    res<-polar.stereographic(xl,rep(max(ylim)*(1-(0.03*(line+0.5))),length(xl)),limit,rotate)
    text.rotate<-ifelse(res$y<0,180-xl-rotate,360-xl-rotate)
    for(i in 1:length(xl))
      if(labels) text(res$x[i],res$y[i],labels=label[i],adj=0.5,srt=text.rotate[i],...)
    if(ticks) polar.segments(xl,rep(max(ylim),length(xl)),xl,rep(max(ylim),length(xl))+ifelse(is.na(par("tck")),-0.01,par("tck"))*diff(ylim)*0.5)
  }
  if(3 %in% side) {
    label<-ifelse(xl>180,paste(360-xl,sep=""),xl)
    label<-ifelse(xl<180,paste(xl,"\u00B0E",sep=""),ifelse(xl==180,paste(label,"\u00B0",sep=""),paste(label,"\u00B0W",sep="")))
    res<-polar.stereographic(xl,rep(min(ylim)*(1+(0.03*(line+0.5))),length(xl)),limit,rotate)
    text.rotate<-ifelse(res$y<0,180-xl-rotate,360-xl-rotate)
    for(i in 1:length(xl))
      if (labels) text(res$x[i],res$y[i],labels=label[i],adj=0.5,srt=text.rotate[i],...)
    if(ticks) polar.segments(xl,rep(min(ylim),length(xl)),xl,rep(min(ylim),length(xl))+ifelse(is.na(par("tck")),-0.01,par("tck"))*diff(ylim)*0.5)
  }
  if(2 %in% side) {
    res<-polar.stereographic(rep(min(xlim),length(yl)),yl,limit,rotate)
    text.rotate<-ifelse(res$x[1]<0,360-min(xlim)-rotate,180-min(xlim)-rotate)
    text.adj<-ifelse(res$x<0,1,0)
    if((text.rotate+90)%%360 > 180) {
      text.rotate<-text.rotate-180
      text.adj<-ifelse(res$y<0,abs(1-text.adj),text.adj)
    }
    label<-paste("  ",abs(yl),"\u00B0S  ",sep="")
    for(i in 1:length(yl))
      if(labels) text(res$x[i],res$y[i],labels=label[i],adj=text.adj[i],srt=text.rotate,...)
    rescale.y<-(yl+90)/360
    rescale.y<-rescale.y/max(rescale.y)*(line+1)
    if(ticks) polar.segments(rep(min(xlim),length(yl)),yl,rep(min(xlim),length(yl))+ifelse(is.na(par("tck")),-0.01,par("tck"))*diff(xlim)*rev(rescale.y),yl)
  }
  if(4 %in% side) {
    res<-polar.stereographic(rep(max(xlim),length(yl)),yl,limit,rotate)
    text.rotate<-ifelse(res$x[1]<0,180-max(xlim)-rotate,360-max(xlim)-rotate)
    text.adj<-ifelse(res$x<0,1,0)
    if((text.rotate+90)%%360 > 180) {
      text.rotate<-text.rotate-180
      text.adj<-ifelse(res$y<0,abs(1-text.adj),text.adj)
    }
    label<-paste("  ",abs(yl),"\u00B0S  ",sep="")
    for(i in 1:length(yl))
      if(labels) text(res$x[i],res$y[i],labels=label[i],adj=text.adj[i],srt=text.rotate,...)
    rescale.y<-(yl+90)/360
    rescale.y<-rescale.y/max(rescale.y)*(line+1)
    if(ticks) polar.segments(rep(max(xlim),length(yl)),yl,rep(max(xlim),length(yl))-ifelse(is.na(par("tck")),-0.01,par("tck"))*diff(xlim)*rev(rescale.y),yl)
  }
  par(xpd=F)
  if(xlim[1]==0 & xlim[2]==360) {
    x<-seq(min(xlim),max(xlim),length=n)
    y<-seq(max(ylim),max(ylim),length=n)
  } else {
    x<-c(seq(min(xlim),max(xlim),length=n),seq(max(xlim),max(xlim),length=n),seq(max(xlim),min(xlim),length=n),seq(min(xlim),min(xlim),length=n))
    y<-c(seq(min(ylim),min(ylim),length=n),seq(min(ylim),max(ylim),length=n),seq(max(ylim),max(ylim),length=n),seq(max(ylim),min(ylim),length=n))
  }
  res<-polar.stereographic(x,y,limit,rotate)
  lines(res$x,res$y, ...)
  invisible()
}
