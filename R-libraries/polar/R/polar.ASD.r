#' polarPlot
#'
#' @export
#'
"polar.ASD" <-
function(lwd=1, lty=1, col=1, fill=NULL, label.col=col, great.circle=T,...) {
# Alistair Dunn: 15 June 2019
  if(!is.null(fill)) {
    polar.polygon(polar.ASD.lines$x,polar.ASD.lines$y, lwd=lwd, lty=lty, err=-1, col=fill, great.circle=great.circle,...)
  }
  polar.lines(polar.ASD.lines$x,polar.ASD.lines$y, lwd=lwd, lty=lty, col=col, err=-1, great.circle=great.circle,...)
invisible()
}
