#' polarPlot
#'
#' @export
#'
"polar.statarea" <-
function(year=2012, labels=T, lwd=1, lty=1, col=1, fill=NULL, label.col=col, great.circle=T,...) {
# Alistair Dunn: 1 October 2004
  n<-.polar$n
  xlim<-.polar$xlim
  ylim<-.polar$ylim
  limit<-.polar$limit
  rotate<-.polar$rotate
  if(year <= 1999) {
    ## subarea.881v1 and subarea.882v1
    stop("Not yet implemented")
    #polar.lines(subarea.881v1$long, subarea.881v1$lat, lwd=lwd, lty=lty, col=col, err=-1, great.circle=great.circle,...)
    #polar.lines(subarea.882v1$long, subarea.882v1$lat, lwd=lwd, lty=lty, col=col, err=-1, great.circle=great.circle,...)
  } else if(year > 1999 & year <= 2004) {
    ## subarea.881v2 and subarea.882v2: Original SSRUs in 881
    if(!is.null(fill)) {
      polar.polygon(subarea.881v2$long, subarea.881v2$lat, lwd=lwd, lty=lty, err=-1, col=fill, great.circle=great.circle,...)
      polar.polygon(subarea.882v2$long, subarea.882v2$lat, lwd=lwd, lty=lty, err=-1, col=fill, great.circle=great.circle,...)
    }
    polar.lines(subarea.881v2$long, subarea.881v2$lat, lwd=lwd, lty=lty, col=col, err=-1, great.circle=great.circle,...)
    polar.lines(subarea.882v2$long, subarea.882v2$lat, lwd=lwd, lty=lty, col=col, err=-1, great.circle=great.circle,...)
    if(labels) {
      x<-c(174.50,174.00,184.50,172.00,185.50,163.00,195.00,205.00,215.00,225.00,235.00,245.00,252.50)
      y<-c(-62.75,-68.50,-68.50,-75.20,-75.00,-67.00,-66.00,-66.00,-66.00,-66.00,-66.00,-66.00,-66.00)
      label<-c("88.1A","88.1B","88.1C","88.1D","88.1E","Balleny Is.","88.2A","88.2B","88.2C","88.2D","88.2E","88.2F","88.2G")
      inside.x <- !is.na(x) & x >= min(xlim) & x <= max(xlim)
      inside.y <- !is.na(y) & y >= min(ylim) & y <= max(ylim)
      inside<-ifelse(is.na(x) | is.na(y) | (inside.x & inside.y), T, F)
      polar.text(x[inside],y[inside],labels=label[inside],...)
    }
  } else if(year > 2004 & year <= 2008) {
    ## subarea.881v3 and subarea.882v3: Modern SSRUs in 881
    if(!is.null(fill)) {
      polar.polygon(subarea.881v3$long, subarea.881v3$lat, lwd=lwd, lty=lty, err=-1, col=fill, great.circle=great.circle,...)
      polar.polygon(subarea.882v3$long, subarea.882v3$lat, lwd=lwd, lty=lty, err=-1, col=fill, great.circle=great.circle,...)
    }
    polar.lines(subarea.881v3$long, subarea.881v3$lat, lwd=lwd, lty=lty, col=col, err=-1, great.circle=great.circle,...)
    polar.lines(subarea.882v3$long, subarea.882v3$lat, lwd=lwd, lty=lty, col=col, err=-1, great.circle=great.circle,...)
    if(labels) {
      x<-c(160.00,174.50,184.50,155.00,165.00,165.00,174.00,175.00,184.50,172.00,184.50,184.50,163.00,195.00,205.00,215.00,225.00,235.00,245.00,252.50)
      y<-c(-62.50,-62.75,-65.00,-67.50,-66.00,-69.30,-68.50,-71.85,-71.40,-75.20,-74.50,-77.00,-67.00,-66.00,-66.00,-66.00,-66.00,-66.00,-66.00,-66.00)
      label<-c("88.1A","88.1B","88.1C","88.1D","88.1E","88.1F","88.1G","88.1H","88.1I","88.1J","88.1K","88.1L","Balleny Is.","88.2A","88.2B","88.2C","88.2D","88.2E","88.2F","88.2G")
      inside.x <- !is.na(x) & x >= min(xlim) & x <= max(xlim)
      inside.y <- !is.na(y) & y >= min(ylim) & y <= max(ylim)
      inside<-ifelse(is.na(x) | is.na(y) | (inside.x & inside.y), T, F)
      polar.text(x[inside],y[inside],labels=label[inside],...)
    }
  } else if(year > 2008 & year <= 2011) {
    # subarea.881v4 and subarea.882v4: Added 881M
    if(!is.null(fill)) {
      polar.polygon(subarea.881v4$long, subarea.881v4$lat, lwd=lwd, lty=lty, err=-1, col=fill, great.circle=great.circle,...)
      polar.polygon(subarea.882v4$long, subarea.882v4$lat, lwd=lwd, lty=lty, err=-1, col=fill, great.circle=great.circle,...)
    }
    polar.lines(subarea.881v4$long, subarea.881v4$lat, lwd=lwd, lty=lty, col=col, err=-1, great.circle=great.circle,...)
    polar.lines(subarea.882v4$long, subarea.882v4$lat, lwd=lwd, lty=lty, col=col, err=-1, great.circle=great.circle,...)
    if(labels) {
      x<-c(160.00,174.50,184.50,155.00,165.00,165.00,174.00,175.00,184.50,175.00,184.50,184.50,166.5,163.00,195.00,205.00,215.00,225.00,235.00,245.00,252.50)
      y<-c(-62.50,-62.75,-65.00,-67.50,-66.00,-69.30,-68.50,-71.85,-71.40,-75.20,-74.50,-77.00,-76.00,-67.00,-66.00,-66.00,-66.00,-66.00,-66.00,-66.00,-66.00)
      label<-c("88.1A","88.1B","88.1C","88.1D","88.1E","88.1F","88.1G","88.1H","88.1I","88.1J","88.1K","88.1L","88.1M","Balleny Is.","88.2A","88.2B","88.2C","88.2D","88.2E","88.2F","88.2G")
      inside.x <- !is.na(x) & x >= min(xlim) & x <= max(xlim)
      inside.y <- !is.na(y) & y >= min(ylim) & y <= max(ylim)
      inside<-ifelse(is.na(x) | is.na(y) | (inside.x & inside.y), T, F)
      polar.text(x[inside],y[inside],labels=label[inside],...)
    }
  } else if(year >= 2012) {
    # subarea.881v5 and subarea.882v5: Revised 88.2 boundaries
    if(!is.null(fill)) {
      polar.polygon(subarea.881$long, subarea.881$lat, lwd=lwd, lty=lty, err=-1, col=fill, great.circle=great.circle,...)
      polar.polygon(subarea.882$long, subarea.882$lat, lwd=lwd, lty=lty, err=-1, col=fill, great.circle=great.circle,...)
    }
    polar.lines(subarea.881$long, subarea.881$lat, lwd=lwd, lty=lty, col=col, err=-1, great.circle=great.circle,...)
    polar.lines(subarea.882$long, subarea.882$lat, lwd=lwd, lty=lty, col=col, err=-1, great.circle=great.circle,...)
    if(labels) {
      x<-c(160.00,174.50,184.50,155.00,165.00,165.00,174.00,175.00,184.50,175.00,184.50,184.50,166.50,163.00,195.00,205.00,215.00,225.00,235.00,245.00,252.50,230.00,230.00)
      y<-c(-62.50,-62.75,-65.00,-67.50,-66.00,-69.30,-68.50,-71.85,-71.40,-75.20,-74.50,-77.00,-76.00,-67.00,-66.00,-66.00,-72.00,-72.00,-72.00,-72.00,-72.00,-66.00,-62.00)
      label<-c("88.1A","88.1B","88.1C","88.1D","88.1E","88.1F","88.1G","88.1H","88.1I","88.1J","88.1K","88.1L","88.1M","BallenyIs.","88.2A","88.2B","88.2C","88.2D","88.2E","88.2F","88.2G","88.2H","88.2I")
      inside.x <- !is.na(x) & x >= min(xlim) & x <= max(xlim)
      inside.y <- !is.na(y) & y >= min(ylim) & y <= max(ylim)
      inside<-ifelse(is.na(x) | is.na(y) | (inside.x & inside.y), T, F)
      polar.text(x[inside],y[inside],labels=label[inside],col=label.col,...)
    }
  }
invisible()
}
