#' polarPlot
#'
#' @export
#'
"polar.grid" <-
function(side=c(1,2),at,lcol=2,...) {
# Alistair Dunn: 1 October 2004
  n<-.polar$n
  xlim<-.polar$xlim
  ylim<-.polar$ylim
  limit<-.polar$limit
  rotate<-.polar$rotate
  x<-c(seq(min(xlim),max(xlim),length=n),seq(max(xlim),max(xlim),length=n),seq(max(xlim),min(xlim),length=n),seq(min(xlim),min(xlim),length=n))
  y<-c(seq(min(ylim),min(ylim),length=n),seq(min(ylim),max(ylim),length=n),seq(max(ylim),max(ylim),length=n),seq(max(ylim),min(ylim),length=n))
  if(missing(at)) {
    xl<-pretty(.polar$xlim)
  } else {
    xl<-at[[1]]
  }
  xl<-xl[xl >= min(xlim) & xl <= max(xlim)]
  if(missing(at)) {
    yl<-pretty(ylim)
  } else {
    yl<-at[[2]]
  }
  yl<-yl[yl >= min(ylim) & yl <= max(ylim)]
  if(2 %in% side) for(i in 1:length(x)) lines(polar.stereographic(rep(xl[i],2),c(min(y),max(y)),limit,rotate),col=lcol,...)
  if(1 %in% side)  for(i in 1:length(y)) lines(polar.stereographic(seq(min(x),max(x),length=n),rep(yl[i],n),limit,rotate),col=lcol,...)
  invisible()
}


