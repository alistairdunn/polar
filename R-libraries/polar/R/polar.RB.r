#' polarPlot
#'
#' @export
#'
"polar.RB" <-
function(year=2018, lwd=1, lty=1, col=1, fill=NULL, label.col=col, great.circle=T,...) {
# Alistair Dunn: 15 June 2019
  if(year <= 2017) {
    stop("Not yet implemented\n")
  } else if(year == 2018) {
    ## RBs in 2019/19 season
	cat("Research Blocks in the 2018/19 season\n")
    if(!is.null(fill)) {
      polar.polygon(polar.RB.lines$long[polar.RB.lines$season==2018],polar.RB.lines$lat[polar.RB.lines$season==2018], lwd=lwd, lty=lty, err=-1, col=fill, great.circle=great.circle,...)
    }
    polar.lines(polar.RB.lines$long[polar.RB.lines$season==2018],polar.RB.lines$lat[polar.RB.lines$season==2018], lwd=lwd, lty=lty, col=col, err=-1, great.circle=great.circle,...)
  } else if(year > 2019) {
    stop("Not yet implemented\n")
  }
invisible()
}
