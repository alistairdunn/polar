#' polarPlot
#'
#' @export
#'
"polar.locator" <-
function(...) {
# Alistair Dunn: 1 October 2004
  RES <- locator(...)
  limit <- .polar$limit
  rotate <- .polar$rotate
  RES<-polar.stereographic.inverse(RES$x,RES$y,limit,rotate)
  return(RES)
}

