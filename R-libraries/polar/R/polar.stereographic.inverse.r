#' polarPlot
#'
#' @export
#'
"polar.stereographic.inverse" <-
function(x,y,limit=c(-90,0),rotate)
{
  if((length(x)/length(y)) %% 1 != 0 & (length(y)/length(x)) %% 1 != 0) stop("x and y are of incompatible lengths")
  deg.to.rad <- pi/180
  RHO <- sqrt(x*x + y*y + .Machine$double.eps)
  LON.RAD<- acos(-y/RHO)
  LON<-LON.RAD/deg.to.rad
  LON<-ifelse(x>=0,360-LON,LON)
  LON<-LON - rotate - 180
  LON<-LON%%360
  lat.rad.max <- limit[2] * deg.to.rad
  LAT<- atan(RHO)/deg.to.rad*2-90
  return(data.frame("long"=LON,"lat"=LAT))
}

