#' polarPlot
#'
#' @export
#'
"polar.arrows" <-
function(x1,y1,x2,y2,...) {
# Alistair Dunn: 1 October 2004
  n<-.polar$n
  xlim<-.polar$xlim
  ylim<-.polar$ylim
  limit<-.polar$limit
  rotate<-.polar$rotate
  res1<-polar.stereographic(x1,y1,limit,rotate)
  res2<-polar.stereographic(x2,y2,limit,rotate)
  arrows(res1$x,res1$y,res2$x,res2$y,...)
}
