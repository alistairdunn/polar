#' polarPlot
#'
#' @export
#'
"polar.polygon" <-
function(x,y,...,great.circle=T) {
# Alistair Dunn: 1 October 2004
  n<-.polar$n
  xlim<-.polar$xlim
  ylim<-.polar$ylim
  limit<-.polar$limit
  rotate<-.polar$rotate
  if(great.circle) {
    res<-polar.polygon.great.circle(x,y,...)
  } else {
    res<-polar.stereographic(x,y,limit,rotate)
    polygon(res$x,res$y,...)
  }
  invisible(res)
}
