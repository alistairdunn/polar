#' polarPlot
#'
#' @export
#'
"polar.CCAMLR" <- 
function(RSRMPA=T,RB=T,ASD=T,world=T,...) {
# Alistair Dunn: July 2019
  polar.plot(xlim=c(0,360),ylim=c(-43,-85))
  polar.grid(at=list(c(0,45,90,135,180,225,270),c(-50,-60,-70,-80)),lcol="grey60")
  if(world) polar.WorldCoastline()
  if(RSRMPA) polar.RSRMPA(fill=rgb(0,0,0,0.4))
  if(RB) polar.RB(fill="red")
  if(ASD) polar.ASD(col="blue",fill=rgb(0,0,0,0.2))
  polar.coastline(fill = T)
  polar.clip()
  polar.axis(side=1,line=1,at=c(0,90,180,270))
  invisible()
}
