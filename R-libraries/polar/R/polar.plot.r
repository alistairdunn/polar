#' polar high level plotting function
#'
#' High level map plotting function for the lands and oceans in the polar region
#' region. The plotting parameters, set in polar.plot, are saved in the list .polar
#' in the global environment, for use as required with some low level plotting
#' functions.
#'
#' @author Alistair Dunn
#' @param xlim vector containing the plot x limits in decimal degrees with longitudes in the range 0 to 360.
#' @param ylim vector containing the plot y limits in decimal degrees with longitudes in the range 0 to 360.
#' @param rotate logical to indicate if the plot has North upwards, or is rotated to that the centre of the map points upwards.
#' @param n value to use for the number of points to interpolate when converting latitude lines into great circles
#' @param resize reswizes the plot on the page as a proportion of par()$pin. Defaults to 1.
#' @param ... other graphics parameters to be passed to plotting functions inside \code{polar}. For example, axis tick label colour is
#'      set by \code{col.axis}, character expansion sizes of axis title and main title are set by \code{cex.lab} and \code{cex.main}
#' @return Returns no value but the side effect is to draw the plot area.
#'
#' @export
#'
"polar.plot" <-
function(xlim=c(120,245),ylim=c(-55,-81),limit=c(-90,0),rotate=T,n=NULL,resize=1) {
# Alistair Dunn: 1 October 2004
  if(is.null(n)) n<-max(floor(abs(diff(range(xlim)))/2.5),80)
  if(xlim[1]==0 & xlim[2]==360) {
    x<-seq(min(xlim),max(xlim),length=n)
    y<-seq(max(ylim),max(ylim),length=n)
  } else {
    x<-c(seq(min(xlim),max(xlim),length=n),seq(max(xlim),max(xlim),length=n),seq(max(xlim),min(xlim),length=n),seq(min(xlim),min(xlim),length=n))
    y<-c(seq(min(ylim),min(ylim),length=n),seq(min(ylim),max(ylim),length=n),seq(max(ylim),max(ylim),length=n),seq(max(ylim),min(ylim),length=n))
  }
  if(rotate) rotate<- -(min(xlim)+abs(diff(xlim))/2)
  else rotate<-0
  res<-polar.stereographic(x,y,limit,rotate)
  xl <- 1
  yl <- 1
  x.range <- abs(diff(range(res$x)))
  y.range <- abs(diff(range(res$y)))
  if(x.range >= y.range) {
    yl <- y.range/x.range
  } else {
    xl <- x.range/y.range
  }
  Snew <- min(par()$pin[1]/xl, par()$pin[2]/yl)
  ressize<-min(resize,1)
  par(pin = c(Snew * xl * resize, Snew * yl * resize))
  par(xaxs="i",yaxs="i")
  plot(0,0,type="n",axes=F,xlab="",ylab="",xlim=range(res$x),ylim=range(res$y))
  lines(res$x,res$y)
  .polar<<-list(xlim=xlim,ylim=ylim,n=n,limit=limit,rotate=rotate)
  invisible()
}

#library(polar)
#polar.plot(xlim=c(0,360),ylim=c(-45,-81))
