#' polarPlot
#'
#' @export
#'
"polar.symbols" <-
function(x, y, ... , add=TRUE)
{
# Alistair Dunn: 1 October 2004
  n<-.polar$n
  xlim<-.polar$xlim
  ylim<-.polar$ylim
  limit<-.polar$limit
  rotate<-.polar$rotate
  res<-polar.stereographic(x,y,limit,rotate)
  symbols(res$x,res$y,add,err=-1,...)
  invisible()
}
