#' polarPlot
#'
#' @export
#'
"polar.polygon.great.circle" <-
function (x, y, ..., delta=0.005)
{
  n <- .polar$n
  xlim <- .polar$xlim
  ylim <- .polar$ylim
  limit <- .polar$limit
  rotate <- .polar$rotate
  res <- data.frame(x = x[1], y = y[1])
  NArow <- data.frame(x = NA, y = NA)
  if (length(x) != length(y)) stop("the lengths of x and y must be equal")
  if (length(x) < 2) stop("the lengths of x and y must be at least 2")
  for (i in 2:length(x)) {
    if (is.na(x[i]) || is.na(y[i])) {
      res <- rbind(res, data.frame(x = x[i], y = y[i]))
    } else if (is.na(x[i - 1]) || is.na(y[i - 1])) {
      res <- rbind(res, data.frame(x = x[i], y = y[i]))
    } else if (abs(x[i] - x[i - 1]) < delta) {
      res <- rbind(res, data.frame(x = x[i], y = y[i]))
    } else {
      X <- seq(x[i - 1], x[i], length = n)
      Y <- seq(y[i - 1], y[i], length = n)
      res <- rbind(res, data.frame(x = X, y = Y))
    }
  }
  res <- polar.stereographic(res$x, res$y, limit, rotate)
  polygon(res$x, res$y, ...)
  invisible(res)
}
