#' polarPlot
#' Add a pie chart to a polar plot
#'
#' @description #   Draws pie charts at specified locations on a map drawn by polar().
#' @param x, y  numeric vectors of coordinates where the text labels should be written. If the length of x and y differs, the shorter one is recycled.
#' @usage polar.pie(x, y, z, size = 0.2, density=-1, angle = 45, border = T, col = rainbow(length(z)), start.angle=0, ...)
#' @param x, y x, y give the location of the centre of the pie charts. x and y is the latitude and longitude of the centre
#' @param z a vector of relative sizes of each slice (unscaled)
#' @param size diameter of the pie as a proportion of the length of the x axis
#' @export
#'
"polar.pie" <-
function(x, y, z, size = 0.2, density = -1, angle = 45, border = T, col = rainbow(length(z)), start.angle = 0)
{
# x and y is the pie graph location, z is a vector of slice sizes
# Alistair Dunn: 1 October 2004
  n<-.polar$n
  xlim<-.polar$xlim
  ylim<-.polar$ylim
  limit<-.polar$limit
  rotate<-.polar$rotate
  res<-polar.stereographic(x,y,limit,rotate)
  size <- 0.5 * size * diff(par()$usr[c(1, 2)])
  tc <- rev((0:360)/180 * pi)
  circle <- data.frame(x = size * cos(tc + pi/2 - start.angle/180 * pi), y = size * sin(tc + pi/2 - start.angle/180 * pi))
  z <- c(0, cumsum(round(z/sum(z) * 360, digits = 0)))
  new.circle <- data.frame(x = 0, y = 0)
  for(i in 2:length(z)) new.circle <- rbind(new.circle, circle[z[i - 1]:z[i],  ], c(0, 0), c(NA, NA))
  polygon(new.circle$x + res$x, new.circle$y + res$y, density = density, angle = angle, border = border, col = col)
  if(border) lines(new.circle$x + res$x, new.circle$y + res$y)
  invisible()
}
