#' polarPlot
#'
#' @export
#'
"polar.RSRMPA" <-
function(zones=c(),...,label=F,fill=NULL,add=T) {
  if(length(zones)>0) {
    if(any(polar.RSRMPA.lines$zone %in% zones)) {
      res<-polar.RSRMPA.lines[polar.RSRMPA.lines$zone %in% zones,]
    } else {
      stop(paste("RSRMPA zone ",zones," not found",sep=""))
    }
  } else {
    res<-polar.RSRMPA.lines
  }
  if(add) {
    if(!is.null(fill)) {
      polar.polygon(res$long,res$lat,col=fill,great.circle=T,...)
    }
    polar.lines(res$long,res$lat,great.circle=T,...)
    if(label) {
      x<-c(167,  163,182,  182, 153)
      y<-c(-75,-60.5,-68,-74.2,-63)
      label<-c("GPZ(i)","GPZ(ii)","GPZ(iii)","SRZ","KRZ")
      inside.x <- !is.na(x) & x >= min(.polar$xlim) & x <= max(.polar$xlim)
      inside.y <- !is.na(y) & y >= min(.polar$ylim) & y <= max(.polar$ylim)
      inside<-ifelse(is.na(x) | is.na(y) | (inside.x & inside.y), T, F)
      polar.text(x[inside],y[inside],labels=label[inside],adj=0,...)
    }
 }
  invisible(res)
}

