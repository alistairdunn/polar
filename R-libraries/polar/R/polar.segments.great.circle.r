#' polarPlot
#'
#' @export
#'
"polar.segments.great.circle" <-
function(x1,y1,x2,y2,...) {
# Alistair Dunn: 1 October 2004
  n<-.polar$n
  xlim<-.polar$xlim
  ylim<-.polar$ylim
  limit<-.polar$limit
  rotate<-.polar$rotate
  if(length(x1)<2) {
    if(!is.na(x1[1]) && !is.na(x2[i]) && (x1[i]-x2[i]) < 0.005) N<-4
    else N=n
    X<-seq(x1,x2,length=N)
    Y<-seq(y1,y2,length=N)
    res<-polar.stereographic(X,Y,limit,rotate)
    lines(res$x,res$y,...)
  } else {
    for(i in 1:length(x1)) {
      if(!is.na(x1[1]) && !is.na(x2[i]) && (x1[i]-x2[i]) < 0.005) N<-4
      else N=n
      X<-seq(x1[i],x2[i],length=N)
      Y<-seq(y1[i],y2[i],length=N)
      res<-polar.stereographic(X,Y,limit,rotate)
      lines(res$x,res$y,...)
    }
  }
  invisible()
}
