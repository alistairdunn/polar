#' polarPlot
#'
#' @export
#'
"polar.clip" <-
function(col="white") {
# Alistair Dunn: 1 October 2004
  n<-.polar$n
  xlim<-.polar$xlim
  ylim<-.polar$ylim
  limit<-.polar$limit
  rotate<-.polar$rotate
  if(xlim[1]==0 & xlim[2]==360) {
    x<-seq(min(xlim),max(xlim),length=n)
    y<-seq(max(ylim),max(ylim),length=n)
  } else {
    x<-c(seq(min(xlim),max(xlim),length=n),seq(max(xlim),max(xlim),length=n),seq(max(xlim),min(xlim),length=n),seq(min(xlim),min(xlim),length=n))
    y<-c(seq(min(ylim),min(ylim),length=n),seq(min(ylim),max(ylim),length=n),seq(max(ylim),max(ylim),length=n),seq(max(ylim),min(ylim),length=n))
  }
  add<-par()$usr
  add[1]<-add[1]*ifelse(add[1]<0,1.3,0.3)
  add[2]<-add[2]*ifelse(add[2]<0,0.3,1.3)
  add[3]<-add[3]*ifelse(add[3]<0,1.1,0.9)
  add[4]<-add[4]*ifelse(add[4]<0,0.9,1.1)
  res<-polar.stereographic(x,y,limit,rotate)
  x<-c(res$x,add[1],add[2],add[2],add[1],add[1])
  y<-c(res$y,add[3],add[3],add[4],add[4],add[3])
  polygon(x,y,col=col,border=NA)
}
