#' polarPlot
#'
#' @export
#'
"polar.text" <-
function(x, y, ...)
{
# Alistair Dunn: 1 October 2004
  n<-.polar$n
  xlim<-.polar$xlim
  ylim<-.polar$ylim
  limit<-.polar$limit
  rotate<-.polar$rotate
  res<-polar.stereographic(x,y,limit,rotate)
  text(res$x, res$y, ...)
  invisible()
}
