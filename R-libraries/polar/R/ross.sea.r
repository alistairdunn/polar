#' polarPlot
#'
#' @export
#'
"ross.sea" <-
function(all = T, statarea = T, MPA=F, MPA.col=rgb(0,0,0,0.1),label = T, cex=0.7, statarea.col="gray50", depth=NULL, ...)
# Alistair Dunn: 1 October 2004
# changed defaults for the plot: to get the old one, use
# ross.sea(F,T,T,1.0,1)
# June 2019: Added MPA switch
{
  if(all)
    polar.plot(xlim = c(145, 258), ylim = c(-59, -81))
  else polar.plot(xlim = c(145, 195), ylim = c(-59, -81))
  if(MPA) {
    polar.RSRMPA(fill=MPA.col)
  }  
  if(statarea)
    polar.statarea(labels = label, cex=cex, col=statarea.col, ...)
  polar.coastline(fill = T)
  if(!is.null(depth)) {
    for(i in 1:length(depth)) {
      polar.depth(depth[i],col="gray70")
    }
  }
  polar.clip()
  polar.axis(cex=cex,...)
  invisible()
}



