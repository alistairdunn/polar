#' polarPlot
#'
#' @export
#'
"polar.abline"<-
function(h,v,n=.polar$n,...) {
  # Alistair Dunn 16 May 2007
  if(missing(v) & missing(h)) stop("Either h or v must be specified")
  if(!missing(v) & !missing(h)) stop("Only one of  h or v can be specified")
  if(!missing(h)) {
    x<-seq(.polar$xlim[1],.polar$xlim[2],length=n)
    y<-rep(h,length(x))
    polar.lines(x,y,...)
  }
  if(!missing(v)) {
    y<-seq(.polar$ylim[1],.polar$ylim[2],length=n)
    x<-rep(v,length(y))
    polar.lines(x,y,...)
  }
  invisible()
}
